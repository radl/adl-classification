from sklearn.tree import ExtraTreeClassifier
from typing import Union
import pandas as pd
import numpy as np
import joblib


class Model:
    """Classifier Model"""

    def __init__(self, model: ExtraTreeClassifier = None) -> None:
        """Main constructor

        Args:
            model: Instance of a classifier, is created if None
        """
        self._model: ExtraTreeClassifier = Model.get_classifier() if model is None else model

    @staticmethod
    def get_classifier() -> ExtraTreeClassifier:
        """Return a new instance of the default classifier

        Returns:
            A new instance of ExtraTreeClassifier
        """
        return ExtraTreeClassifier()

    def get_model(self) -> ExtraTreeClassifier:
        """Getter for the model / classifier

        Returns:
            The model / classifier
        """
        return self._model

    @staticmethod
    def from_dataset(data: Union[pd.DataFrame, np.ndarray], label: Union[pd.Series, np.ndarray]) -> 'Model':
        """Create a new instance of Model and train it given a dataset

        Args:
            data: DataFrame of data (or ndarray)
            label: Series of labels (or ndarray)

        Returns:
            New instance of Model trained
        """
        model = Model.get_classifier()
        model.fit(data, label)
        return Model(model)

    def save(self, folder_path: str, filename: str = 'model') -> None:
        """Saves the model to a given file

        Args:
            folder_path: Path of the folder
            filename: Name of the file
        """
        joblib.dump(self._model, folder_path + '/' + filename + '.joblib')

    @staticmethod
    def from_file(folder_path: str, filename: str = 'model') -> 'Model':
        """Load a saved instance of Model

        Args:
            folder_path: Path of the folder
            filename: Name of the file

        Returns:
            Loaded instance of Model
        """
        model = joblib.load(folder_path + '/' + filename + '.joblib')
        return Model(model)

    def predict(self, data: pd.DataFrame, indexes: pd.DataFrame = None) -> pd.DataFrame:
        """Create a new instance of Model and train it given a dataset

        Args:
            data: DataFrame of data (or ndarray)
            indexes: Optional DataFrame of indexes to add to the prediction result

        Returns:
            DataFrame with the prediction result
        """
        df = pd.DataFrame(self._model.predict(data), columns=['label'])

        if indexes is not None:
            df = pd.concat([indexes, df], axis=1)
            df.set_index(list(indexes.columns), inplace=True)

        return df
