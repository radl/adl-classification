from preprocessing import load, unpack, drop_label
from .model import Model
from .test import *


def load_dataset(path: str):
    """Load a given dataset and format it into data / label

    Args:
        path: Path of the dataset

    Returns:
        A tuple [DataFrame, Series] for data / label
    """
    df = load(path)
    return unpack(df), df['label']


def create_model(dataset_path: str) -> Model:
    """Creates a model and train it from a given dataset

    Args:
        dataset_path: Path of the dataset

    Returns:
        Trained model
    """
    index, data, label = unpack(load(dataset_path))
    return Model.from_dataset(data, label)


def load_model(folder_path: str, filename: str = 'model') -> Model:
    """Load a saved model

    Args:
        folder_path: Path of the dataset
        filename: Name of the dataset

    Returns:
        Loaded model
    """
    return Model.from_file(folder_path, filename)


def test_model(dataset_path: str, test_size: float = 0.3, plot: bool = False) -> None:
    """Function executing all the tests given a dataset

    Args:
        dataset_path: Path of the dataset
        test_size: Size of testing dataset for percentage split test
        plot: Whether to plot the confusion matrix for percentage split test
    """
    # Loading dataset
    index, data, label = unpack(load(dataset_path))

    # Testing
    # acc = cross_validation_test(data, label)
    cm, rep = percentage_split_test(data, label, test_size=test_size, plot=plot)

    # Saving the results
    cm.to_csv('./csv/confusion_matrix.csv')
    rep.to_csv('./csv/report.csv')


def test_against_dataset(training_dataset_path: str, test_dataset_path: str, plot: bool = False) -> None:
    """Function executing test against specific dataset

    Args:
        training_dataset_path: Path of the training dataset
        test_dataset_path: Path of the training dataset
        plot: Whether to plot the confusion matrix for percentage split test
    """
    # Loading both dataset
    training_index, training_data, training_label = unpack(drop_label(load(training_dataset_path), 'toileting'))
    test_index, test_data, test_label = unpack(load(test_dataset_path))

    # Testing
    cm, rep = dataset_test(training_data, training_label, test_data, test_label, plot=plot)

    # Saving the results
    cm.to_csv('./csv/confusion_matrix.csv')
    rep.to_csv('./csv/report.csv')
