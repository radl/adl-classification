from sklearn.metrics import plot_confusion_matrix, classification_report, confusion_matrix
from sklearn.model_selection import StratifiedKFold, cross_val_score, train_test_split
import matplotlib.pyplot as plt
from typing import Tuple
import pandas as pd

from .model import Model

# Setting options on pandas for better readability
pd.set_option("display.precision", 3)
pd.set_option('display.expand_frame_repr', False)

# Setting options on plt for better readability
fig, ax = plt.subplots(figsize=(8, 8))


def cross_validation_test(data: pd.DataFrame, label: pd.Series, n_folds: int = 10) -> float:
    """Test the accuracy of the model by cross-validation over a given number of folds

    Args:
        data: DataFrame of data
        label: Series of labels
        n_folds: Number of folds for the cross-validation

    Returns:
        Average accuracy over the folds
    """
    model = Model.get_classifier()
    cv = StratifiedKFold(n_splits=n_folds, shuffle=True, random_state=1)
    scores = cross_val_score(model, data, label, scoring='accuracy', cv=cv, n_jobs=-1, error_score='raise')
    mean = scores.mean()
    std = scores.std()

    print("Cross-validation test (" + str(n_folds) + " - folds) accuracy: %0.2f (+/- %0.2f)" % (mean, std * 2))

    return mean


def display_confusion_matrix(model: Model, data: pd.DataFrame, label: pd.Series) -> None:
    """Plot the confusion matrix given a trained model and testing data / label

    Args:
        model: Trained model
        data: DataFrame of data
        label: Series of labels
    """
    display = plot_confusion_matrix(model.get_model(), data, label, ax=ax, values_format='.2f',
                                    xticks_rotation='vertical',
                                    cmap=plt.cm.Blues, normalize='true')
    display.ax_.set_title("Confusion Matrix")
    plt.show()


def test_with_dataset(data_train: pd.DataFrame, label_train: pd.Series, data_test: pd.DataFrame,
                      label_test: pd.Series, plot: bool = False) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """Test the model by splitting the dataset from a given percentage

    Will do a stratified shuffled split, then train and test the model.
    A report and confusion matrix is then generated

    Args:
        data_train: DataFrame of data
        label_train: Series of labels
        data_test: DataFrame of data
        label_test: Series of labels
        plot: Whether to plot the confusion matrix

    Returns:
        A tuple of DataFrames for both the confusion matrix and the report
    """
    # Create model, train, predict
    model = Model.from_dataset(data_train, label_train)
    label_pred = model.predict(data_test).values
    indexes = label_train.drop_duplicates()
    indexes.name = None

    # Create confusion matrix and report
    cm = pd.DataFrame(confusion_matrix(label_test, label_pred, normalize='true'), index=indexes, columns=indexes)
    rep = pd.DataFrame(classification_report(label_test, label_pred, output_dict=True))

    if plot:
        # Plot the confusion matrix
        display_confusion_matrix(model, data_test, label_test)

    return cm, rep


def percentage_split_test(data: pd.DataFrame, label: pd.Series, test_size: float = 0.3, plot: bool = False) \
        -> Tuple[pd.DataFrame, pd.DataFrame]:
    """Test the model by splitting the dataset from a given percentage

    Will do a stratified shuffled split, then train and test the model.
    A report and confusion matrix is then generated

    Args:
        data: DataFrame of data
        label: Series of labels
        test_size: Percentage of test over training data (0. - 1.)
        plot: Whether to plot the confusion matrix

    Returns:
        A tuple of DataFrames for both the confusion matrix and the report
    """
    data_train, data_test, label_train, label_test = train_test_split(data, label, test_size=test_size,
                                                                      shuffle=True, stratify=label, random_state=0)

    cm, rep = test_with_dataset(data_train, label_train, data_test, label_test, plot=plot)

    print("Percentage split test (" + str(test_size * 100) + " %) report:\n" + str(rep) + "\n")
    print("Percentage split test (" + str(test_size * 100) + " %) confusion matrix:\n" + str(cm))

    return cm, rep


def dataset_test(data_train: pd.DataFrame, label_train: pd.Series, data_test: pd.DataFrame,
                 label_test: pd.Series, plot: bool = False) -> Tuple[pd.DataFrame, pd.DataFrame]:
    cm, rep = test_with_dataset(data_train, label_train, data_test, label_test, plot=plot)

    print("Dataset test report:\n" + str(rep) + "\n")
    print("Dataset test confusion matrix:\n" + str(cm))

    return cm, rep
