from experiment import build_dataset_from_all_experiment
from processing import *

build_dataset_from_all_experiment(nominal=False)

test_model('./csv/all/nominal_clean/all.csv', plot=True, test_size=0.99)
test_against_dataset('./csv/exp0/nominal_clean/all.csv', './csv/exp1/nominal_clean/all.csv', plot=True)
