import pandas as pd
import math

from utils import *


def label_to_code(label: str) -> int:
    """Convert a given label string to a code number

    Args:
        label: Label as string

    Returns:
        Label as code number
    """
    if label == 'sleeping':
        return 0
    elif label == 'sitting':
        return 1
    elif label == 'toileting':
        return 2
    elif label == 'showering':
        return 3
    elif label == 'cooking':
        return 4
    elif label == 'eating':
        return 5
    elif label == 'dressing':
        return 6
    elif label == 'presence':
        return 7
    elif label == 'absence':
        return 8
    else:  # Undefined
        return -1


def get_label_value(label_logs: LabelLogs, time: DateTime, nominal: bool = True) -> Union[str, int]:
    """Looks for a label value at a given time

    Args:
        label_logs: List of labeled logs
        time: Time for the synchronized label
        nominal: Whether the label should be nominal or numeric

    Returns:
        Code number for the label
    """
    value: str = label_logs['label'][0]

    if time < DateTime.from_str(label_logs['time'][0]):
        raise Exception("Given time is earlier than the first log")

    # Search for the time range at a given time and look at its label
    for i in range(1, len(label_logs['label'])):
        if time < DateTime.from_str(label_logs['time'][i]):
            value = label_logs['label'][i-1]
            break
        elif i == len(label_logs['label']) - 1:
            value = label_logs['label'][-1]

    return value if nominal else label_to_code(value)


def format_labels(label_logs: LabelLogs, from_time: DateTime, to_time: DateTime, nominal: bool = True) -> Labels:
    """Re-sample and format the labeled logs to every seconds of a time range

    Args:
        label_logs: List of labeled logs
        from_time: Beginning of time range
        to_time: Ending of time range
        nominal: Whether the labels should be nominal or numeric

    Returns:
        Formatted and re-sampled Labels
    """
    if len(label_logs['serialNumber']) > 0:
        labels: Labels = {'serialNumber': label_logs['serialNumber'][0], 'labels': []}

        # For every seconds of the time range
        for i in range(0, math.ceil(from_time.delta(to_time))):
            time = from_time.add_seconds(i)
            labels['labels'].append({'time': time.value, 'value': get_label_value(label_logs, time, nominal)})

        return labels
    else:
        raise Exception("Given labels object is empty")


def create_df(labels: Labels):
    """Create a DataFrame from formatted Labels

    Args:
        labels: Formatted Labels from which to create DataFrame

    Returns:
        DataFrame object with all formatted labels
    """
    df: pd.DataFrame = pd.DataFrame(labels['labels'])
    df.rename(columns={'value': 'label'}, inplace=True)
    df.insert(0, 'serialNumber', labels['serialNumber'])
    df.set_index(['time', 'serialNumber'], inplace=True)
    return df


def generate(df: pd.DataFrame, from_time: DateTime, to_time: DateTime, nominal: bool = True) -> pd.DataFrame:
    """Generates time series of labels given a list of labeled logs

    Takes a list of labeled logs and re-sample them to every seconds of a given time range

    Args:
        df: DataFrame with log labels
        from_time: Beginning of time range
        to_time: Ending of time range
        nominal: Whether the labels should be nominal or numeric

    Returns:
        DataFrame with a labeled log for every seconds
    """
    return create_df(format_labels(df.reset_index().to_dict(orient='list'), from_time, to_time, nominal))
