import json
import math

from utils import *


#####################################################################
# Util functions

def print_node(node: Union[GraphQLNode, Node]) -> None:
    """Pretty print a Node object

    Args:
        node: The node object to print
    """
    print(json.dumps(node, indent=3, default=lambda o: str(o)))


#####################################################################

def get_closest_time_log(logs: List[GraphQLLog], time: DateTime) -> Optional[GraphQLLog]:
    """Search for the log which time is closest to the given time

    Args:
        logs: List of GraphQL sensor logs
        time: Time to compare the logs with

    Returns:
        If found, returns the log which time is closest to the one given, otherwise None
    """
    previous_delta: float = 0
    index: int = -1

    for j, log in enumerate(logs):
        # Evaluate the closest log by calculating the delta (difference)
        delta = time.delta(DateTime.from_str(log['time']))

        if j > 0 and delta > previous_delta:
            break

        previous_delta = delta
        index += 1

    return logs[index] if index != -1 else None


def get_connectivity_value(info: WatchOptions, logs: List[GraphQLLog], time: DateTime) -> bool:
    """Examine sensor logs to determine if the Node was connected at a given time

    Args:
        info: Containing max timeout for connectivity
        logs: GraphQL sensor log to examine
        time: Time of the observation

    Returns:
        Whether the device was connected at the given time
    """
    # Search for the closest log to a given time
    log: Optional[GraphQLLog] = get_closest_time_log(logs, time)
    connected: bool = False

    if log is not None:
        delta: float = time.delta(DateTime.from_str(log['time']))
        connected = delta < info['timeout']

    return connected


def format_connectivity_logs(sensors: Dict[str, List[GraphQLLog]], info: WatchOptions,
                             from_time: DateTime, to_time: DateTime) -> List[ConnectivityLog]:
    """Generate connectivity logs for a given time range

    Examine logs of a given sensor and determine for each second whether the Node was connected

    Args:
        sensors: Group of sensors and their logs
        info: Connectivity watch info (which sensor to watch and max timeout for connectivity)
        from_time: Beginning of the time range
        to_time: Ending of the time range

    Returns:
        List of connectivity logs for every seconds of the time range
    """
    arr: List[ConnectivityLog] = []

    # Order the logs by time ASC
    sensors[info['sensor']].sort(key=lambda x: x['time'])

    # Evaluate whether the Node was connected for every seconds of the time range
    for i in range(0, math.ceil(from_time.delta(to_time))):
        time: DateTime = from_time.add_seconds(i)
        new_log: ConnectivityLog = {'time': time.value,
                                    'value': get_connectivity_value(info, sensors[info['sensor']], time)}
        arr.append(new_log)

    return arr


def get_log_value(sensor: str, logs: List[GraphQLLog], time: DateTime) -> int:
    """Format the value of a sensor log for a given time

    Search in the sensor logs for a log closest to a given time.
    If not found, will take a default value (previous known value).
    Also converts bool log value to int

    Args:
        sensor: key for accessing default value
        logs: List of all sensor logs
        time: Time for the synchronized log

    Returns:
        Log value for the given time
    """
    # Search for the closest log to a given time
    log: Optional[GraphQLLog] = get_closest_time_log(logs, time)
    value: int = Defaults.get(sensor)

    if log is not None:
        if isinstance(log['value'], bool):
            # Format bool value into int and makes sure to not re-sample HIGH value over a larger time range
            value = (1 if log['value'] else 0) if time.delta(DateTime.from_str(log['time'])) < 1 else 0
        else:
            value = log['value']

    # Set default sensor value to previous value
    # Except for motionDetector which default should be left to 0
    if sensor != 'motionDetector':
        Defaults.set(sensor, value)

    return value


def format_logs(sensor: str, logs: List[GraphQLLog], connectivity: List[ConnectivityLog]) -> List[Log]:
    """Re-sample and format the logs of a given sensor

    Base the re-sampling time on connectivity logs and defaults the values to 0 if the Node is assumed disconnected.
    Re-sample by searching for the closest log to each time value in the sensor logs,
    otherwise will take a default value.

    Args:
        sensor: Name of the sensor
        logs: List of all sensor logs
        connectivity: List of connectivity logs

    Returns:
        List of formatted logs for the sensor
    """
    arr: List[Log] = []

    # Order the logs by time ASC
    logs.sort(key=lambda x: x['time'])

    for connectivity_log in connectivity:
        time: DateTime = DateTime(connectivity_log['time'])
        new_log: Log = {'time': time.value, 'value': 0}

        if connectivity_log['value']:
            new_log['value'] = get_log_value(sensor, logs, time)

        arr.append(new_log)

    return arr


def format_node(node: GraphQLNode, from_time: DateTime, to_time: DateTime, info: WatchOptions) -> Node:
    """Format the GraphQL result for a given node

    Converts the time string of all logs into datetime object.
    Synchronise the logs between each other, by re-sampling them to each seconds of the given time range.
    Also generates a list of connectivity logs.

    Args:
        node: GraphQL result for a specific Node
        from_time: Beginning of time range
        to_time: Ending of time range
        info: Connectivity watch info (which sensor to watch and max timeout for connectivity)

    Returns:
        Formatted Node object
    """
    connectivity: List[ConnectivityLog] = format_connectivity_logs(node['sensors'], info, from_time, to_time)
    return {'serialNumber': node['serialNumber'],
            'sensors': {sensor: format_logs(sensor, logs, connectivity) for (sensor, logs) in node['sensors'].items()},
            'connected': connectivity}
