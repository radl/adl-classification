from shutil import rmtree
from os import mkdir, path
from glob import glob

from .data import *
from .label import *
from preprocessing.graphql import *


NODES = ["RADL-SN-0001", "RADL-SN-0002", "RADL-SN-0003", "RADL-SN-0004", "RADL-SN-0005"]
"""List of Sensor Nodes serial number"""

NOM_ARR = ['sitting', 'sleeping', 'dressing', 'cooking', 'eating', 'showering', 'toileting', 'presence', 'absence']
"""List of nominal labels"""

NUM_ARR = range(0, 9)
"""List of numeric labels"""

RANGES: Dict[str, Range] = {
    'temperature': {'min': 10, 'max': 40},
    'humidity': {'min': 0, 'max': 100},
    'co2Level': {'min': 0, 'max': 30000},
    'lightIntensity': {'min': 0, 'max': 1000},
    'soundLevel': {'min': 0, 'max': 150},
    'motionDetector': {'min': 0, 'max': 1}
}
"""Sensors value range for normalization"""


def rm_folder(folder_path: str) -> None:
    """Remove a given folder if exists

    Args:
        folder_path: Path of the folder
    """
    if path.exists(folder_path) and path.isdir(folder_path):
        rmtree(folder_path)


def build_folder_path(folder: str, prefix: str = '', empty: bool = False) -> str:
    """Format a folder path, as well as emptying the folder if asked

    Args:
        folder: Name of the folder (e.g. data or labeled)
        prefix: Name of possible containing folder (e.g. 'exp1' if './csv/exp1/data')
        empty: Whether to empty the folder

    Returns:
        Formatted folder path
    """
    folder_path = './csv'

    if not path.exists(folder_path):
        mkdir(folder_path)

    if prefix != '':
        folder_path += '/' + prefix

        if not path.exists(folder_path):
            mkdir(folder_path)

    folder_path += '/' + folder

    if empty:
        rm_folder(folder_path)

    if not path.exists(folder_path):
        mkdir(folder_path)

    return folder_path


def build_file_path(folder_path: str, filename: str) -> str:
    """Format a file path, given its folder path and filename

    Args:
        folder_path: Path of the folder
        filename: Name of the file

    Returns:
        Formatted file path
    """
    return folder_path + '/' + filename + '.csv'


def concat_nodes(folder_path: str) -> None:
    """Concatenates data between all nodes

    Given a folder, will concatenate data between all nodes for each sub-folder individually

    Args:
        folder_path: Path of the folder
    """
    # Get the list of sub-folders
    print("Concatenating nodes: " + folder_path)

    # Create a list of DataFrames for each node data in the sub-folder
    df_arr = [load(file_path) for file_path in glob(folder_path + '/[!all]*.csv')]

    if len(df_arr) > 0:
        # Concatenate and save into a new file (all.csv)
        df = pd.concat(df_arr)
        save(build_file_path(folder_path, 'all'), df, reset=True)


def gather_data(time_ranges: List[TimeRange], interval: int, prefix: str):
    """Gather nodes data by querying the GraphQL API for a given range

    The request is split between time ranges for handling smaller amount of data at a time.
    It is also split into smaller intervals to not ask too much at a time to the GraphQL API

    Args:
        time_ranges: List of time ranges to split the amount of handled data
        interval: Interval in seconds to split the API requests
        prefix: Name of containing folder (e.g. 'exp1' if './csv/exp1/data')
    """
    # Split data gathering into multiple files and folders
    folder_path = build_folder_path('data', prefix, empty=True)

    for node in NODES:
        for i, time_range in enumerate(time_ranges):

            file_path = build_file_path(folder_path, node)

            # Handles all partial responses by appending them to their corresponding CSV file
            for df in fetch(API.node_fetcher(node), time_range['from_time'], time_range['to_time'], interval=interval):
                save(file_path, df)

    concat_nodes(folder_path)


def gen_labels(time_ranges: List[TimeRange], nominal: bool, prefix: str):
    """Generate time series of logs for the labeling

    Will look into the logs folder and generate time series of labels for every seconds of a time range

    Args:
        time_ranges: List of time ranges to split the amount of handled data
        nominal: Whether the labels should be nominal or numeric
        prefix: Name of containing folder (e.g. 'exp1' if './csv/exp1/logs')
    """
    entry_folder_path = build_folder_path('logs', prefix, empty=False)
    output_folder_path = build_folder_path('nominal_labels' if nominal else 'numeric_labels', prefix, empty=True)

    for node in NODES:
        entry_path = build_file_path(entry_folder_path, node)
        output_path = build_file_path(output_folder_path, node)
        print("Generating labels: " + output_path)

        df = load(entry_path)

        for i, time_range in enumerate(time_ranges):
            new_df = generate(df, time_range['from_time'], time_range['to_time'], nominal)
            save(output_path, new_df)

    concat_nodes(output_folder_path)


def label_data(nominal: bool, prefix: str):
    """Concatenate data and labels

    Args:
        nominal: Whether the labels is nominal or numeric
        prefix: Name of containing folder (e.g. 'exp1' if './csv/exp1/data')
    """
    folder_data = build_folder_path('data', prefix, empty=False)
    folder_labels = build_folder_path('nominal_labels' if nominal else 'numeric_labels', prefix, empty=False)
    folder_labeled = build_folder_path('nominal_labeled' if nominal else 'numeric_labeled', prefix, empty=True)

    for node in NODES:
        data_path = build_file_path(folder_data, node)
        labels_path = build_file_path(folder_labels, node)
        output_path = build_file_path(folder_labeled, node)
        print("Labeling data: " + output_path)

        df = pack(data=load(data_path), label=load(labels_path))
        save(output_path, df)

    concat_nodes(folder_labeled)


def clean_labeled(nominal: bool, prefix: str):
    """Clean the data by removing all the logs where either the label is undefined or the device was disconnected

    Args:
        nominal: Whether the labels is nominal or numeric
        prefix: Name of containing folder (e.g. 'exp1' if './csv/exp1/nominal_clean')
    """
    folder_labeled = build_folder_path('nominal_labeled' if nominal else 'numeric_labeled', prefix, empty=False)
    folder_clean = build_folder_path('nominal_clean' if nominal else 'numeric_clean', prefix, empty=True)

    for node in NODES:
        entry_path = build_file_path(folder_labeled, node)
        output_path = build_file_path(folder_clean, node)
        print("Cleaning data: " + output_path)

        df = clean(load(entry_path))
        save(output_path, df)

    concat_nodes(folder_clean)


def normalize_clean(nominal: bool, prefix: str):
    """Normalize all the sensors values from the clean dataset

    Args:
        nominal: Whether the labels is nominal or numeric
        prefix: Name of containing folder (e.g. 'exp1' if './csv/exp1/nominal_clean')
    """
    folder_clean = build_folder_path('nominal_clean' if nominal else 'numeric_clean', prefix, empty=False)
    folder_normalized = build_folder_path('nominal_normalized' if nominal else 'numeric_normalized', prefix, empty=True)

    for node in NODES:
        entry_path = build_file_path(folder_clean, node)
        output_path = build_file_path(folder_normalized, node)
        print("Normalizing data: " + output_path)

        df = normalize(load(entry_path), RANGES)
        save(output_path, df)

    concat_nodes(folder_normalized)


def gen_mono_label(nominal: bool, prefix: str):
    """Generates mono label dataset for each labels of a cleaned dataset

    Args:
        nominal: Whether the labels is nominal or numeric
        prefix: Name of containing folder (e.g. 'exp1' if './csv/exp1/nominal_clean')
    """

    entry_folder = build_folder_path('nominal_clean' if nominal else 'numeric_clean', prefix, empty=False)
    output_folder = build_folder_path('nominal_mono' if nominal else 'numeric_mono', prefix, empty=True)
    df = load(build_file_path(entry_folder, 'all'))

    for label in NOM_ARR if nominal else NUM_ARR:
        filename = label if nominal else str(label)
        print("Creating mono label dataset: " + filename)
        save(build_file_path(output_folder, filename), mono_label(df, label))


def pre_processing(time_ranges: List[TimeRange], interval: int,
                   fetch_data: bool = False, nominal: bool = True, prefix: str = ''):
    """Function executing all the steps for re-creating the dataset from scratch

    Args:
        time_ranges: List of time ranges to split the amount of handled data
        interval: Interval in seconds to split the API requests
        fetch_data: Whether to fetch the data from the GraphQL API
        nominal: Whether the labels should be nominal or numeric
        prefix: Name of containing folder (e.g. 'exp1' if './csv/exp1/data')
    """
    if fetch_data:
        gather_data(time_ranges, interval, prefix)

    gen_labels(time_ranges, nominal, prefix)
    label_data(nominal, prefix)
    clean_labeled(nominal, prefix)
    normalize_clean(nominal, prefix)
    gen_mono_label(nominal, prefix)
