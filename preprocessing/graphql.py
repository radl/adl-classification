from graphqlclient import GraphQLClient
from urllib.error import URLError
import json

from utils import *


# GraphQL API Endpoint
ENDPOINT: str = 'http://localhost:4000/api/graphql'


#####################################################################
# Format GraphQL operation strings

def op_nodes(from_time: str, to_time: str) -> str:
    """Format GraphQL query string for all nodes

    Args:
        from_time: Beginning of time range
        to_time: Ending of time range

    Returns:
        Formatted GraphQL operation
    """
    return '''
        query {{
            nodes {{
                serialNumber
                sensors(from: "{}", to: "{}") {{
                    temperature {{
                        time
                        value
                    }}
                    humidity {{
                        time
                        value
                    }}
                    co2Level {{
                        time
                        value
                    }}
                    lightIntensity {{
                        time
                        value
                    }}
                    motionDetector {{
                        time
                        value
                    }}
                }}
            }}
        }}
    '''.format(from_time, to_time)


def op_node(serial_number: str, from_time: str, to_time: str) -> str:
    """Format GraphQL query string for a specific node

    Args:
        serial_number: Sensor node serial number
        from_time: Beginning of time range
        to_time: Ending of time range

    Returns:
        Formatted GraphQL operation
    """
    return '''
        query {{
            node(serialNumber: "{}") {{
                serialNumber
                sensors(from: "{}", to: "{}") {{
                    temperature {{
                        time
                        value
                    }}
                    humidity {{
                        time
                        value
                    }}
                    co2Level {{
                        time
                        value
                    }}
                    lightIntensity {{
                        time
                        value
                    }}
                    motionDetector {{
                        time
                        value
                    }}
                }}
            }}
        }}
    '''.format(serial_number, from_time, to_time)


class API:
    """GraphQL API Client"""

    _client: GraphQLClient = GraphQLClient(ENDPOINT)
    """Graphql Client connected to the System API"""

    @staticmethod
    def log(msg: str) -> None:
        """API Logger

        Args:
            msg: Message to log
        """
        print("GQL> " + msg)

    @staticmethod
    def _execute(op: str, op_name: str) -> Dict[str, Any]:
        """Execute GraphQL operation

        Args:
            op: GraphQL operation string
            op_name: name of the GraphQL operation

        Raises:
            SystemExit: Raise system exit when the GraphQL API is not reachable

        Returns:
            GraphQL API response formatted as a Dict object
        """
        try:
            result = json.loads(API._client.execute(op))

            # Prints any error
            if 'errors' in result.keys():
                API.log("Graphql Operation Failed (" + op_name + ")")
                print(str(result['errors']))

            # Return GraphQL API Response
            if 'data' in result.keys():
                return result['data']

        except URLError:
            API.log("API not reachable")
            raise SystemExit

    @staticmethod
    def nodes(from_time: DateTime, to_time: DateTime) -> List[GraphQLNode]:
        """GraphQL Query - nodes

        Args:
            from_time: Beginning of time range
            to_time: Ending of time range

        Returns:
            List of GraphQL Node objects
        """
        API.log("Fetching nodes (from: " + str(from_time) + " / to: " + str(to_time) + ")")
        result: GraphQLResponseNodes = API._execute(op_nodes(str(from_time), str(to_time)), 'nodes')
        return result['nodes']

    @staticmethod
    def node(serial_number: str, from_time: DateTime, to_time: DateTime) -> GraphQLNode:
        """GraphQL Query - node

        Args:
            serial_number: Sensor node serial number
            from_time: Beginning of time range
            to_time: Ending of time range

        Returns:
            GraphQL Node object
        """
        API.log("Fetching node " + serial_number + " (from: " + str(from_time) + " / to: " + str(to_time) + ")")
        result: GraphQLResponseNode = API._execute(op_node(serial_number, str(from_time), str(to_time)), 'node')
        return result['node']

    @staticmethod
    def nodes_fetcher() -> Callable[[DateTime, DateTime], List[GraphQLNode]]:
        """ Generate a function for fetching the nodes given a time range

        Returns:
            Function for fetching the nodes given a time range
        """
        def get_nodes(from_time: DateTime, to_time: DateTime) -> List[GraphQLNode]:
            return API.nodes(from_time, to_time)

        return get_nodes

    @staticmethod
    def node_fetcher(serial_number: str) -> Callable[[DateTime, DateTime], List[GraphQLNode]]:
        """ Generate a function for fetching a specific node given a time range

        Args:
            serial_number: Serial number of the node to fetch

        Returns:
            Function for fetching the node given a time range
        """
        def get_node(from_time: DateTime, to_time: DateTime) -> List[GraphQLNode]:
            return [API.node(serial_number, from_time, to_time)]

        return get_node
