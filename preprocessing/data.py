from os.path import isfile
import pandas as pd
import numpy as np
import math

from .node import *


def create_df(node: Node) -> pd.DataFrame:
    """Creates a DataFrame from all the logs of a formatted Node

    Args:
        node: Formatted Node from which to create the DataFrame

    Returns:
        DataFrame object with all sensor logs
    """
    df_arr: List[pd.DataFrame] = []

    # Creates DataFrame from connectivity logs
    connected_df: pd.DataFrame = pd.DataFrame(node['connected'])
    connected_df.rename(columns={'value': 'connected'}, inplace=True)
    connected_df.set_index('time', inplace=True)
    df_arr.append(connected_df)

    # Creates a DataFrame from each sensor logs
    for sensor, logs in node['sensors'].items():
        sensor_df: pd.DataFrame = pd.DataFrame(node['sensors'][sensor])
        sensor_df.rename(columns={'value': sensor}, inplace=True)
        sensor_df.set_index('time', inplace=True)
        df_arr.append(sensor_df)

    # Concatenates all the DataFrames
    df = pd.concat(df_arr, axis=1)
    df.insert(0, 'serialNumber', node['serialNumber'])
    df.reset_index(inplace=True)
    df.set_index(['time', 'serialNumber'], inplace=True)
    return df


def clean(df: pd.DataFrame) -> pd.DataFrame:
    """Clean the DataFrame from rows with unknown labels or disconnected status

    Args:
        df: DataFrame to clean

    Returns:
        Cleaned DataFrame object
    """
    # Search for row indexes where either the label is undefined or the device was disconnected
    new_df = df.copy()

    if 'connected' in new_df.columns:
        new_df.drop(new_df[~new_df['connected']].index, inplace=True)
        new_df.drop('connected', axis=1, inplace=True)

    if 'label' in new_df.columns:
        if new_df['label'].dtype == np.int:
            new_df.drop(new_df[new_df['label'] == -1].index, inplace=True)
        else:
            new_df.drop(new_df[new_df['label'] == 'none'].index, inplace=True)

    return new_df


def normalize(df: pd.DataFrame, ranges: Dict[str, Range]) -> pd.DataFrame:
    """Normalize the sensors values given their (min - max) range

    Does the normalization directly to the given DataFrame

    Args:
        df: DataFrame with the sensors values to normalize
        ranges: Dictionary of (min - max) ranges for each sensors

    Returns:
        DataFrame with normalized sensors
    """
    new_df = df.copy()

    for sensor, sensor_range in ranges.items():
        if sensor in new_df.columns:
            new_df[sensor] = (new_df[sensor] - sensor_range['min']) / (sensor_range['max'] - sensor_range['min'])

    return new_df


def test_d(x):
    print(x)
    print(x['label'])
    print(x['label'] == 'sitting')
    return 'label' if (x['label'] == 'sitting') else 'other'


def mono_label(df: pd.DataFrame, label: Union[str, int]) -> pd.DataFrame:
    new_df = df.copy()

    if 'label' in new_df.columns:
        if new_df['label'].dtype == np.int and isinstance(label, int):
            new_df['label'] = new_df['label'].apply(lambda x: 0 if x == label else 1)
        elif new_df['label'].dtype == np.object and isinstance(label, str):
            new_df['label'] = new_df['label'].apply(lambda x: label if x == label else 'other')
        else:
            raise Exception("Type mismatch between DataFrame labels and given label")
    else:
        raise Exception("No labels found in DataFrame")

    return new_df


def drop_label(df: pd.DataFrame, label: Union[str, int]) -> pd.DataFrame:
    return df[df['label'] != label]


def pack(data: Union[pd.DataFrame, None] = None, index: Union[pd.DataFrame, None] = None,
         label: Union[pd.DataFrame, None] = None) -> Union[pd.DataFrame, None]:
    """Pack a DataFrame by concatenating index, data and label

    Args:
        index: DataFrame of index
        data: DataFrame of data
        label: DataFrame of label

    Returns:
        Concatenated DataFrame
    """
    df_arr: List[pd.DataFrame] = [df for df in [index, data, label] if df is not None]

    if len(df_arr) == 0 or (len(df_arr) == 1 and index is not None):
        return None
    else:
        df = pd.concat(df_arr, axis=1)

        if index is None:
            return df
        else:
            return df.set_index(list(index.columns))


def unpack(df: pd.DataFrame) -> Tuple[Union[pd.DataFrame, None], Union[pd.DataFrame, None], Union[pd.Series, None]]:
    """Unpack a DataFrame by extracting indexes and labels

    Args:
        df: DataFrame from which to unpack indexes and labels

    Returns:
        Tuple of DataFrames for index / data / label
    """
    index = df.index.to_frame(index=False)
    data = df.reset_index(drop=True)
    label = None

    if 'label' in df.columns:
        label = data['label']
        data.drop(columns='label', inplace=True)

    if len(data.columns) == 0:
        data = None

    if list(index.columns) == [0]:
        index = None

    return index, data, label


def fetch(fetch_data: Callable[[DateTime, DateTime], List[GraphQLNode]],
          from_time: DateTime, to_time: DateTime, interval: int = -1) -> Iterable[pd.DataFrame]:
    """Fetch the sensor logs of Node / Nodes and format it to a DataFrame

    Args:
        fetch_data: Function for fetching the Node / Nodes
        from_time: Beginning of time range
        to_time: Ending of time range
        interval: If set, will split the fetching to this specified time range

    Returns:
        Iterable over split DataFrame with all the re-sampled and formatted sensor logs
    """
    info: WatchOptions = {'sensor': 'lightIntensity', 'timeout': 3}
    full_interval: int = math.ceil(from_time.delta(to_time))
    inter: int = full_interval if interval < 0 else interval
    loops: int = math.ceil(full_interval / inter)

    # Loops only when `interval` is set
    for i in range(0, loops):
        # Calculate a partial time range depending on `interval`
        from_partial_time: DateTime = from_time.add_seconds(i * inter)
        partial_inter: int = inter if from_partial_time.add_seconds(inter) < to_time \
            else math.ceil(from_partial_time.delta(to_time))
        to_partial_time: DateTime = from_partial_time.add_seconds(partial_inter)

        # Fetch Node / Nodes, format its sensors logs and create DataFrame for each node
        for node in fetch_data(from_partial_time, to_partial_time):
            yield create_df(format_node(node, from_partial_time, to_partial_time, info))


def save(file_path: str, df: pd.DataFrame, reset: bool = False) -> None:
    """Save a given DataFrame to a CSV file

    Args:
        file_path: Path of the file
        df: DataFrame to save
        reset: Whether to reset to the given file
    """
    append = not reset and isfile(file_path)

    df.to_csv(file_path, mode='a' if append else 'w', header=not append)


def load(file_path: str) -> pd.DataFrame:
    """Load a DataFrame from a CSV file

    Args:
        file_path: Path of the file

    Returns:
        Loaded DataFrame
    """
    df = pd.read_csv(file_path, parse_dates=True, date_parser=lambda d: DateTime.from_str(d).value)
    df.set_index(['time', 'serialNumber'], inplace=True)
    return df
