from preprocessing import pre_processing
from utils import *


INTERVAL: int = 1800
"""Interval in seconds for splitting the GraphQL fetching request (30min)"""

TIME_RANGES_ARR: List[List[TimeRange]] = [
    [
        {
            'from_time': DateTime.from_str("2020-07-11T18:00:00+01:00"),
            'to_time': DateTime.from_str("2020-07-12T00:00:00+01:00")
        },
        {
            'from_time': DateTime.from_str("2020-07-12T00:00:00+01:00"),
            'to_time': DateTime.from_str("2020-07-13T00:00:00+01:00")
        },
        {
            'from_time': DateTime.from_str("2020-07-13T00:00:00+01:00"),
            'to_time': DateTime.from_str("2020-07-14T00:00:00+01:00")
        },
        {
            'from_time': DateTime.from_str("2020-07-14T00:00:00+01:00"),
            'to_time': DateTime.from_str("2020-07-15T00:00:00+01:00")
        },
        {
            'from_time': DateTime.from_str("2020-07-15T00:00:00+01:00"),
            'to_time': DateTime.from_str("2020-07-16T00:00:00+01:00")
        },
        {
            'from_time': DateTime.from_str("2020-07-16T00:00:00+01:00"),
            'to_time': DateTime.from_str("2020-07-16T09:30:00+01:00")
        }
    ],
    [
        {
            'from_time': DateTime.from_str("2020-07-16T17:00:00+01:00"),
            'to_time': DateTime.from_str("2020-07-17T00:00:00+01:00")
        },
        {
            'from_time': DateTime.from_str("2020-07-17T00:00:00+01:00"),
            'to_time': DateTime.from_str("2020-07-18T00:00:00+01:00")
        },
        {
            'from_time': DateTime.from_str("2020-07-18T00:00:00+01:00"),
            'to_time': DateTime.from_str("2020-07-19T00:00:00+01:00")
        },
        {
            'from_time': DateTime.from_str("2020-07-19T00:00:00+01:00"),
            'to_time': DateTime.from_str("2020-07-20T00:00:00+01:00")
        },
        {
            'from_time': DateTime.from_str("2020-07-20T00:00:00+01:00"),
            'to_time': DateTime.from_str("2020-07-20T12:00:00+01:00")
        }
    ]
]
"""Matrix of time ranges, to split the data handling (avg. of 1 time range per day) for a given experiment"""


def build_dataset_from_experiment(index: int, fetch: bool = False, nominal=True):
    if 0 <= index < len(TIME_RANGES_ARR):
        pre_processing(TIME_RANGES_ARR[index], INTERVAL, fetch_data=fetch, nominal=nominal, prefix='exp'+str(index))
    else:
        raise Exception("Experiment index out of range")


def build_dataset_from_all_experiment(fetch: bool = False, nominal=True):
    for i in range(0, len(TIME_RANGES_ARR)):
        build_dataset_from_experiment(i, fetch=fetch, nominal=nominal)
