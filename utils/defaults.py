from .typed import *


class Defaults:
    """Sensors default value

    Holds a dictionary with default values for each sensor
    (Default = 0 if not set)
    """

    _defaults: Dict[str, int] = {}
    """Dictionary containing all the default values"""

    @staticmethod
    def get(key: str) -> int:
        """Getter for a default value

        Args:
            key: Key related to the default value to return

        Returns:
            Default value to the related key
        """
        return Defaults._defaults[key] if key in Defaults._defaults.keys() else 0

    @staticmethod
    def set(key: str, value: int) -> None:
        """Setter for a default value

        Args:
            key: Key related to the default value
            value: New default value
        """
        Defaults._defaults[key] = value
