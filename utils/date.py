from datetime import datetime, timedelta
from dateutil import parser
import math


class DateTime(object):
    """Date Time with ISO 8601 formatting"""

    def __init__(self, value: datetime) -> None:
        """Main constructor

        Args:
            value: datetime value
        """
        self._value: datetime = value

    @property
    def value(self) -> datetime:
        """Getter for the datetime value"""
        return self._value

    @staticmethod
    def from_str(value: str) -> 'DateTime':
        """Parse an ISO 8601 date string to datetime

        Args:
            value date string with ISO 8601 format

        Returns:
            New instance of DateTime from parsed value
        """
        return DateTime(parser.isoparse(value))

    def add_seconds(self, seconds: int) -> 'DateTime':
        """Add seconds to the current DateTime (not inplace)

        Args:
            seconds: number of seconds to add

        Returns:
            New DateTime instance with the added seconds
        """
        return DateTime(self._value + timedelta(0, seconds))

    def delta(self, other: 'DateTime') -> float:
        """Calculate the delta in seconds between two DateTime

        Args:
            other: The other DateTime

        Returns:
            Positive difference between the two DateTimes in floating seconds
        """
        return math.fabs((self._value - other.value).total_seconds())

    def __gt__(self, other: 'DateTime') -> bool:
        """Implementing 'greater than' operator (>)

        Args:
            other: The other DateTime (right operand of the comparison)

        Returns:
            Whether the current DateTime (left operand) is greater than the other
        """
        return self._value > other.value

    def __lt__(self, other: 'DateTime') -> bool:
        """Implementing 'less than' operator (<)

        Args:
            other: The other DateTime (right operand of the comparison)

        Returns:
            Whether the current DateTime (left operand) is less than the other
        """
        return self._value < other.value

    def __str__(self) -> str:
        """Stringify a datetime to an ISO 8601 date string

        Returns:
            Date string with ISO 8601 format
        """
        return self._value.astimezone().isoformat()
