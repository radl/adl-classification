from typing_extensions import TypedDict
from datetime import datetime
from .date import DateTime
from typing import *


class TimeRange(TypedDict):
    """Type for Graphql Log"""
    from_time: DateTime
    to_time: DateTime


class GraphQLLog(TypedDict):
    """Type for Graphql Log"""
    time: str
    value: Union[int, bool]


class WatchOptions(TypedDict):
    """Connectivity """
    sensor: str   # Which sensor to check for connectivity
    timeout: int  # Max timeout without any new log for the watched sensor


class GraphQLLog(TypedDict):
    """Type for Graphql Log"""
    time: str
    value: Union[int, bool]


class ConnectivityLog(TypedDict):
    """Type for Connectivity Log"""
    time: datetime
    value: bool


class Log(TypedDict):
    """Type for formatted Log"""
    time: datetime
    value: int


class LogStr(TypedDict):
    """Type for formatted Log with str value"""
    time: datetime
    value: str


class GraphQLNode(TypedDict):
    """Type for Graphql Node"""
    serialNumber: str
    sensors: Dict[str, List[GraphQLLog]]


class Node(TypedDict):
    """Type for formatted Node"""
    serialNumber: str
    sensors: Dict[str, List[Log]]
    connected: List[ConnectivityLog]


class GraphQLResponseNodes(TypedDict):
    """Type for Response to GraphQL Query - nodes"""
    nodes: List[GraphQLNode]


class GraphQLResponseNode(TypedDict):
    """Type for Response to GraphQL Query - node"""
    node: GraphQLNode


class Range(TypedDict):
    """Type for Integer Range"""
    min: int
    max: int


class LabelLogs(TypedDict):
    """Type for Label Logs"""
    time: List[str]
    serialNumber: List[str]
    label: List[str]


class Labels(TypedDict):
    """Type for formatted Labels"""
    serialNumber: str
    labels: List[Union[LogStr, Log]]
